#! /bin/bash

# Check if user is root
user=$(whoami)

if [ "$user" != "root" ]; then
    echo "Please run this as root"
    exit 1
fi


courseSrc="$(pwd)/course-service"
userOauthSrc="$(pwd)/user-and-oauth-sevice"
logSrc="$(pwd)/log-server"

# Create necessary folders
mkdir -p "/var/lib/yx/proxy/cache"
mkdir -p "/var/lib/grades"
mkdir -p "/etc/yx/course"
mkdir -p "/etc/yx/proxy"
mkdir -p "/etc/yx/user"
mkdir -p "/etc/yx/oauth"
mkdir -p "/etc/yx/log"
mkdir -p "/var/log/yx/course"
mkdir -p "/var/log/yx/oauth"
mkdir -p "/var/log/yx/user"
mkdir -p "/var/log/yx/log"

# Copy configuration files
cp -r "$courseSrc/dev-config/course" "/etc/yx/"
cp -r "$courseSrc/dev-config/proxy" "/etc/yx/"
cp -r "$userOauthSrc/dev-config/user" "/etc/yx/"
cp -r "$userOauthSrc/dev-config/oauth" "/etc/yx/"
cp -r "$logSrc/dev-conf/log" "/etc/yx/"

# Change template services
replacePWD()
{
    sed -i 's/[PWD]/$(pwd)/g' "$1.template" > $1
}

replacePWD "log-service.service"
replacePWD "oauth-service.service"
replacePWD "user-service.service"
replacePWD "course-service.service"

# Copy all services to systemd

cp *.service /etc/systemd/system/
