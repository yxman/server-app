#! /bin/bash

# This script is used to build the projet in a convectional manner

pwd=$(pwd)

## Make sure all repos all pulled
echo "Trying to pull all submodules"
git submodule update --init --recursive

## Create build folder
courseBuild="./build/course-service"
oauthBuild="./build/oauth-service"
userBuild="./build/user-service"
logBuild="./build/log-service"

courseSrc="./course-service/"
oauthSrc="./user-and-oauth-sevice/oauth-service/"
userSrc="./course-service/user-service/"
logSrc="./log-server/"


echo "Creating all build directories"
mkdir -p $courseBuild
mkdir -p $oauthBuild
mkdir -p $userBuild
mkdir -p $logBuild

fixCPR()
{
    cd $1/yxlib/lib/cpr/cpr/

    sed '5iSHARED' CMakeLists.txt > CMakeLists.txt
}


build() # Build Folder, CMakeList containing directory, library Folder
{
    # Go to the build Folder
    cd $1
    cmake $2 ; fixCPR $3 && cmake $2  &&  make
}

# build Course-service
echo "Building Course service"
build "$pwd/$courseBuild" "$pwd/$courseSrc" "$pwd/$courseSrc/lib" 

# Build OAuth-service
echo "Build Oauth service"
build "$pwd/$oauthBuild" "$pwd/$oauthSrc" "$pwd/$oauthSrc/../lib" 

# Build User-service
echo "Build User service"
build "$pwd/$userBuild" "$pwd/$userSrc" "$pwd/$userSrc/../lib"

# Build Log-service
echo "Build Log service"
build "$pwd/$logBuild" "$pwd/$logSrc" "$pwd/$logSrc/lib"
